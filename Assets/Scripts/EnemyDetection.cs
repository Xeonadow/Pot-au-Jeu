﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour
{
	public float visionAngle = 20.0f;
	
	private EnemyController controller;
	private SphereCollider range;
	private Transform mesh;

	void Start()
	{
		controller = GetComponentInParent<EnemyController>();
		range = GetComponent<SphereCollider>();
		mesh = transform.GetChild(0);
	}

	private void Update()
	{
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit, range.radius))
			mesh.localScale = new Vector3(hit.distance, hit.distance, hit.distance);
		else
			mesh.localScale = new Vector3(range.radius, range.radius, range.radius);

	}

	void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player" || other.tag == "Clone")
		{
			Vector3 direction = other.ClosestPoint(transform.position) - transform.position;
			
			//if (Vector3.Angle(direction, transform.forward) <= visionAngle / 2.0f)
				//Debug.DrawRay(transform.position, direction, new Color(255.0f, 0.0f, 0.0f));

			RaycastHit hit;
			if (Vector3.Angle(direction, transform.forward) <= visionAngle / 2
			&& Physics.Raycast(transform.position, direction, out hit, range.radius)
			&& (hit.transform.tag == "Player" || hit.transform.tag == "Clone"))
			{
				controller.PlayerFound(other.transform);
				gameObject.SetActive(false);
			}			
		}
	}

}
