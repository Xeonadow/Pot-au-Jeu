﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : Character
{
	public float patrolSpeed;
	public float chaseSpeed;
	public Transform[] patrolPath;

	private NavMeshAgent agent;
	private bool patrolMode = true;
	private int targetMark = 0;
	private Transform target;
	private GameObject detector;

	protected override void Awake()
	{
		base.Awake();
		agent = GetComponent<NavMeshAgent>();
	}

	void Start ()
	{		
		detector = transform.GetChild(0).gameObject;
		agent.speed = patrolSpeed;
		if (patrolPath.Length > 0)
			agent.SetDestination(patrolPath[targetMark].position);
	}
	
	void Update ()
	{
		if (patrolMode)
		{
			if (patrolPath.Length > 0)
			{
				if (agent.remainingDistance <= 1.0f)
				{
					targetMark++;
					if (targetMark >= patrolPath.Length)
						targetMark = 0;
				}
				agent.SetDestination(patrolPath[targetMark].position);
			}
			else if (agent.destination == transform.position)
				transform.rotation = Quaternion.RotateTowards(transform.rotation, origin.rotation, agent.angularSpeed * Time.deltaTime);
		}
		else if (target.gameObject.activeSelf == false)
			BackToPatrol();
		else if (!agent.pathPending)
			agent.SetDestination(target.position);
	}

	void BackToPatrol()
	{
		target = null;
		patrolMode = true;
		detector.SetActive(true);
		agent.speed = patrolSpeed;
		if (patrolPath.Length > 0)
		{
			float smallestDistance = Vector3.Distance(transform.position, patrolPath[0].position);
			float dist;

			targetMark = 0;
			for (int i = 1; i < patrolPath.Length; i++)
			{
				dist = Vector3.Distance(transform.position, patrolPath[i].position);
				if (dist < smallestDistance)
				{
					smallestDistance = dist;
					targetMark = i;
				}
			}

			agent.SetDestination(patrolPath[targetMark].position);
		}
		else
			agent.SetDestination(origin.position);
	}

	public void PlayerFound (Transform player)
	{
		patrolMode = false;
		target = player;
		agent.speed = chaseSpeed;
	}

	public override void Reset()
	{
		//base.Reset();
		agent.Warp(origin.position);
		transform.rotation = origin.rotation;
		patrolMode = true;
		target = null;
		if (patrolPath.Length > 0)
		{
			targetMark = 0;
			agent.SetDestination(patrolPath[targetMark].position);
		}
	}
}
