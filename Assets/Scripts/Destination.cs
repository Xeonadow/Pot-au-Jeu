﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destination : MonoBehaviour {

	private GameController gamecontroller;

	private void Start()
	{
		gamecontroller = FindObjectOfType<GameController>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
			gamecontroller.LevelComplete();
	}
}
