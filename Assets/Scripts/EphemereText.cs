﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EphemereText : MonoBehaviour {

	public float nbSeconds = 3.0f;

	void Start () {
		Time.timeScale = 0.0f;
		StartCoroutine(CloseAfterTime());
	}

	IEnumerator CloseAfterTime()
	{
		yield return new WaitForSecondsRealtime(nbSeconds);
		Time.timeScale = 1.0f;
		gameObject.SetActive(false);
	}
}
