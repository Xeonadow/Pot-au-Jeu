﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clone : Character {

	[HideInInspector]
	public List<PointInTime> path = null;
	private int i = 0;

	void FixedUpdate ()
	{
		if (path == null)
			return ;
		
		if (i >= path.Count)
		{
			gameObject.SetActive(false);
			return ;
		}
		PointInTime cur = path[i];
		transform.position = cur.position;
		transform.rotation = cur.rotation;
		i++;
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Enemy")
		{
			gameObject.SetActive(false);
		}
	}

	public override void Reset()
	{
		base.Reset();
		i = 0;
	}
}
