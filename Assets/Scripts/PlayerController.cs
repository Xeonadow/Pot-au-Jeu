﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Character
{
	public float speed = 20.0f;

	private Vector3 xVec;
	private Vector3 yVec;
	private Vector3 direction;
	private float angle;

	private GameController gameController;
	
	void Start ()
	{
		gameController = FindObjectOfType<GameController>();

		xVec = Vector3.Normalize(new Vector3(1, 0, -1));
		yVec = Vector3.Normalize(new Vector3(1, 0, 1));
		direction = Vector3.zero;
	}

	void Update()
	{
		direction = Input.GetAxisRaw("Horizontal") * xVec + Input.GetAxisRaw("Vertical") * yVec;
		if (direction.sqrMagnitude > 1.0f)
			direction = Vector3.Normalize(direction);
		angle = Vector3.SignedAngle(Vector3.forward, direction, Vector3.up);
	}

	void FixedUpdate()
	{
		transform.position += direction * speed * Time.fixedDeltaTime;
		transform.eulerAngles = new Vector3(0, angle, 0);
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Enemy")
		{
			gameController.PlayerCaught();
		}
	}
}
