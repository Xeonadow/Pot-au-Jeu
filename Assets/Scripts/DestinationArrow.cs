﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationArrow : MonoBehaviour {

	public Transform destination;
	public float wobbleSpeed = 5.0f;
	public float wobbleAmplitude = 0.5f;
	private Transform arrow;

	private Vector3 startPosition;

	void Start () {
		//startPosition = transform.localPosition;
		arrow = transform.GetChild(0);
		startPosition = arrow.localPosition;
	}
	
	void LateUpdate () {
		Vector3 target = new Vector3(destination.position.x, transform.position.y, destination.position.z);
		transform.LookAt(target);

		/*
		Vector3 direction = new Vector3(target.x - transform.parent.position.x, 0.0f, target.z - transform.parent.position.z).normalized;
		Debug.DrawRay(transform.position, direction);
		transform.localPosition = startPosition + transform.forward * Mathf.Sin(Time.time * wobbleSpeed) * wobbleAmplitude;
		*/
		arrow.localPosition = startPosition + new Vector3(0.0f, 0.0f, Mathf.Sin(Time.time * wobbleSpeed) * wobbleAmplitude);
	}
}
