﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
	public Transform player;
	private Vector3 offsetToPlayer;

	void Start ()
	{
		offsetToPlayer = transform.position - player.position;
	}
	
	void LateUpdate () {
		transform.position = player.position + offsetToPlayer;
	}
}
