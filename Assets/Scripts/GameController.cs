﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public Transform player;
	public int nbOfClones = 2;
	public GameObject ClonePrefab;	

	public GameObject caughtPanel;
	public float restartTime = 2.0f;

	public string nextLevelName;

	private List<PointInTime> recordingList = null;
	private int indexToRecordTo = 0;
	//private List<PointInTime>[] pointLists;
	private Clone[] clones;

	void Start ()
	{
		//pointLists = new List<PointInTime>[nbOfClones];
		clones = new Clone[nbOfClones];
		for (int i = 0; i < nbOfClones; i++)
		{
			//pointLists[i] = null;
			clones[i] = Instantiate(ClonePrefab, player.position, player.rotation).GetComponent<Clone>();
			clones[i].gameObject.SetActive(false);
		}
		InitializeRun();
		
	}
	
	void FixedUpdate ()
	{
		recordingList.Add(new PointInTime(player.position, player.rotation));
	}

	void InitializeRun()
	{
		/*for (int i = nbOfClones - 1; i > 0; i--)
		{
			pointLists[i] = pointLists[i - 1];
		}
		if (nbOfClones > 0)
			pointLists[0] = recordingList;*/

		// Shift lists amongst the clones
		/*
		for (int i = nbOfClones - 1; i > 0 ; i--)
		{
			clones[i].path = clones[i - 1].path;
		}
		if (nbOfClones > 0)
			clones[0].path = recordingList.;
		*/

		recordingList = new List<PointInTime>();

		// Activate player and needed clones
		player.gameObject.SetActive(true);
		int j = 0;
		while (j < nbOfClones && clones[j].path != null)
		{
			clones[j].gameObject.SetActive(true);
			j++;
		}

		// Resets position of all characters
		Character[] characters = FindObjectsOfType<Character>();
		for (int i = 0; i < characters.Length; i++)
		{
			characters[i].Reset();
		}
	}

	public void PlayerCaught()
	{
		caughtPanel.SetActive(true);
		player.gameObject.SetActive(false);
		clones[indexToRecordTo].path = recordingList;
		indexToRecordTo = (indexToRecordTo + 1) % nbOfClones;
		StartCoroutine(WaitForRestart());
	}

	IEnumerator WaitForRestart()
	{
		yield return new WaitForSecondsRealtime(restartTime);
		caughtPanel.SetActive(false);
		InitializeRun();
	}

	public void LevelComplete()
	{
		SceneManager.LoadScene(nextLevelName);
	}
}
