﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

	protected PointInTime origin;

	protected virtual void Awake ()
	{
		origin = new PointInTime(transform.position, transform.rotation);
	}

	public virtual void Reset()
	{
		transform.position = origin.position;
		transform.rotation = origin.rotation;
	}
}
