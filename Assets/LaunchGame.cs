﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LaunchGame : MonoBehaviour {

	public string level;
	public bool enableQuit = false;

	void Update () {
		if (Input.GetButton("Submit"))
			SceneManager.LoadScene(level);

		if (Input.GetButton("Cancel"))
			Application.Quit();
	}
}
